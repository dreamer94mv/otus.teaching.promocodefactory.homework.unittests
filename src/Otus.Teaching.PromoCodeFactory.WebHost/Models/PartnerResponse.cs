﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PartnerResponse
    {
        public PartnerResponse()
        {
        }

        public PartnerResponse(Partner x)
        {
            Id = x.Id;
            Name = x.Name;
            NumberIssuedPromoCodes = x.NumberIssuedPromoCodes;
            IsActive = true;
            PartnerLimits = x.PartnerLimits
                .Select(y => new PartnerPromoCodeLimitResponse
                {
                    Id = y.Id,
                    PartnerId = y.PartnerId,
                    Limit = y.Limit,
                    CreateDate = y.CreateDate.ToString("dd.MM.yyyy hh:mm:ss"),
                    EndDate = y.EndDate.ToString("dd.MM.yyyy hh:mm:ss"),
                    CancelDate = y.CancelDate?.ToString("dd.MM.yyyy hh:mm:ss")
                }).ToList();
        }

        public Guid Id { get; set; }

        public bool IsActive { get; set; }

        public string Name { get; set; }

        public int NumberIssuedPromoCodes { get; set; }

        public List<PartnerPromoCodeLimitResponse> PartnerLimits { get; set; }
    }
}