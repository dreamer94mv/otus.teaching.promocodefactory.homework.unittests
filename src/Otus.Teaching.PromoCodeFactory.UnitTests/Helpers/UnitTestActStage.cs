using System;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Helpers
{
    public class UnitTestActStage<T>
    {
        private readonly T _arrangeContext;
        public UnitTestActStage(T arrangeContext)
        {
            _arrangeContext = arrangeContext;
        }

        public async Task<UnitTestAssertStage<T, T1>> ActAsync<T1>(Func<T,Task<T1>> act)
        {
            var actResult = await act(_arrangeContext);
            return new UnitTestAssertStage<T, T1>(_arrangeContext, actResult);
        }
    }

    public class UnitTestActStage
    {
        public UnitTestAssertStage<T1> Act<T1>(Func<T1> act)
        {
            T1 actResult = act();
            return new UnitTestAssertStage<T1>(actResult);
        }
    }

}