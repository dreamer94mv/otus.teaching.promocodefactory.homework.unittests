using System;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Helpers
{
    public class UnitTestAssertStage<TArrangeContext, TActResult>
    {
        private readonly TArrangeContext _arrangeContext;
        private readonly TActResult _actResult;
        public UnitTestAssertStage(TArrangeContext arrangeContext,TActResult actResult )
        {
            _arrangeContext = arrangeContext;
            _actResult = actResult;
        }
        
        public void Assert(Action<TArrangeContext, TActResult> assert)
        {
            assert(_arrangeContext, _actResult);
        }
        
        public async Task AssertAsync(Func<TArrangeContext, TActResult,Task> assert)
        {
            await assert(_arrangeContext, _actResult);
        }
    }
    
    public class UnitTestAssertStage<TActResult>
    {
        private readonly TActResult _actResult;
        public UnitTestAssertStage(TActResult actResult )
        {
            _actResult = actResult;
        }
        
        public void Assert(Action<TActResult> assert)
        {
            assert(_actResult);
        }
        
        public async Task AssertAsync(Func<TActResult,Task> assert)
        {
            await assert(_actResult);
        }
    }

}