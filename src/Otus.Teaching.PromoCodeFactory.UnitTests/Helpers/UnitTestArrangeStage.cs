using System;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Helpers
{
    public class UnitTestArrangeStage
    {
        public UnitTestActStage Arrange(Action action)
        {
            action();
            return new UnitTestActStage();
        }

        public UnitTestActStage<T1> Arrange<T1>(Func<T1> arrange)
        {
            T1 arrangeContext = arrange();
            return new UnitTestActStage<T1>(arrangeContext);
        }
        
        public async Task<UnitTestActStage<T1>> ArrangeAsync<T1>(Func<Task<T1>> arrange)
        {
            T1 arrangeContext = await arrange();
            return new UnitTestActStage<T1>(arrangeContext);
        }
        
        public UnitTestActStage<Tuple<T1,T2>> Arrange<T1,T2>(Func<Tuple<T1,T2>> arrange)
        {
            var arrangeContext = arrange();
            return new UnitTestActStage<Tuple<T1,T2>>(arrangeContext);
        }
    }
}