﻿using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Otus.Teaching.PromoCodeFactory.UnitTests.Helpers;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        
        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }
        
        /// <summary>
        /// Если партнер не найден, то также нужно выдать ошибку 404
        /// </summary>
        [Fact]
        public void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            (new UnitTestBuilder()
                    .CreateTest()
                    .Arrange(() =>
                        SetPartnerPromoCodeLimitAsyncArrangeBuilder
                            .SetupPartnerNotFoundArrange(_partnersRepositoryMock))
                    .ActAsync(arrangeContext =>
                        _partnersController.SetPartnerPromoCodeLimitAsync(arrangeContext.PartnerId,
                            arrangeContext.Request)))
                .ConfigureAwait(false)
                .GetAwaiter()
                .GetResult()
                .Assert((arrangeContext, actResult) => actResult.Should().BeAssignableTo<NotFoundResult>());
        }

        /// <summary>
        /// Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400
        /// </summary>
        [Fact]
        public void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
             (new UnitTestBuilder()
                 .CreateTest()
                 .Arrange(() =>
                     SetPartnerPromoCodeLimitAsyncArrangeBuilder
                         .SetupPartnerIsNotActiveArrange(_partnersRepositoryMock))
                 .ActAsync(arrangeContext => 
                     _partnersController.SetPartnerPromoCodeLimitAsync(arrangeContext.PartnerId,
                         arrangeContext.Request))
                 .ConfigureAwait(false)
                 .GetAwaiter()
                 .GetResult())
                 .Assert((arrangeContext, actResult) => actResult.Should().BeAssignableTo<BadRequestObjectResult>());
        }

        
        /// <summary>
        /// Если партнеру выставляется лимит, то мы должны обнулить количество промокодов,
        /// которые партнер выдал NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется
        /// Сделал только чтобы попробовать Theory. А так бы разделил на два теста
        /// </summary>
        /// <param name="availableLimit"></param>
        [Theory]
        [InlineData(false)]
        [InlineData(true)]
        public void SetPartnerPromoCodeLimitAsync_PartnerDifferentAvailableLimit_CheckedPromocodeCount(bool availableLimit)
        {
            (new UnitTestBuilder()
                .CreateTest()
                .Arrange(() =>
                    SetPartnerPromoCodeLimitAsyncArrangeBuilder.SetupPartnerWithWithoutAvailableLimit(
                        _partnersRepositoryMock, availableLimit))
                .ActAsync(arrangeContext =>
                    _partnersController.SetPartnerPromoCodeLimitAsync(arrangeContext.PartnerId,
                        arrangeContext.Request)))
                .ConfigureAwait(false)
                .GetAwaiter()
                .GetResult()
                .Assert((arrangeContext, actResult) =>
                {
                    actResult.Should().BeAssignableTo<CreatedAtActionResult>();
                    var partnerInfo = _partnersController.GetPartnerAsync(arrangeContext.PartnerId)
                        .ConfigureAwait(false).GetAwaiter().GetResult();
                    partnerInfo.Result.Should().BeAssignableTo<OkObjectResult>();
                    partnerInfo.Result.As<OkObjectResult>().Value.Should().NotBeNull().And
                        .BeAssignableTo<PartnerResponse>(); 
                    if(availableLimit)
                        partnerInfo.Result.As<OkObjectResult>().Value.As<PartnerResponse>().NumberIssuedPromoCodes
                            .Should().Be(0);
                    else
                        partnerInfo.Result.As<OkObjectResult>().Value.As<PartnerResponse>().NumberIssuedPromoCodes
                            .Should().BeGreaterThan(0);
                });
        }

        /// <summary>
        /// При установке лимита нужно отключить предыдущий лимит;
        /// </summary>
        [Fact]
        public void SetPartnerPromoCodeLimitAsync_PartnerNewLimit_OldCancelled()
        {
            (new UnitTestBuilder()
                .CreateTest()
                .Arrange(() =>
                    SetPartnerPromoCodeLimitAsyncArrangeBuilder.SetupPartnerWithExistAndNewLimits(
                        _partnersRepositoryMock))
                .ActAsync(arrangeContext =>
                    _partnersController.SetPartnerPromoCodeLimitAsync(arrangeContext.PartnerId,
                        arrangeContext.Request)))
                .ConfigureAwait(false)
                .GetAwaiter()
                .GetResult()
                .Assert((arrangeContext, actResult) =>
                {
                    actResult.Should().BeAssignableTo<CreatedAtActionResult>();
                    var partnerInfo =  _partnersController.GetPartnerLimitsAsync(arrangeContext.PartnerId)
                        .ConfigureAwait(false).GetAwaiter().GetResult();
                    
                    partnerInfo.Result.Should().BeAssignableTo<OkObjectResult>();
                    partnerInfo.Result.As<OkObjectResult>().Value.Should().NotBeNull().And
                        .BeAssignableTo<List<PartnerPromoCodeLimitResponse>>(); 
                    partnerInfo.Result.As<OkObjectResult>().Value.As<List<PartnerPromoCodeLimitResponse>>()
                        .Count(p => string.IsNullOrEmpty(p.CancelDate)).Should().Be(1);
                });
        }

        /// <summary>
        /// Лимит должен быть больше 0;
        /// </summary>
        /// <param name="newLimitValue">Значение лимита</param>
        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        [InlineData(1)]
        public void SetPartnerPromoCodeLimitAsync_PartnerSetLimit_CheckLimitBound(int newLimitValue)
        {
            (new UnitTestBuilder()
                .CreateTest()
                .Arrange(() => SetPartnerPromoCodeLimitAsyncArrangeBuilder.SetupPartnerWithSpecificLimit(_partnersRepositoryMock, newLimitValue))
                .ActAsync(arrangeContext => _partnersController.SetPartnerPromoCodeLimitAsync(arrangeContext.PartnerId, arrangeContext.Request)))
                .ConfigureAwait(false)
                .GetAwaiter()
                .GetResult()
                .Assert((arrangeContext, actResult) =>
                {
                    if (newLimitValue <= 0)
                        actResult.Should().BeAssignableTo<BadRequestObjectResult>();
                    else
                        actResult.Should().BeAssignableTo<CreatedAtActionResult>();
                });
        }
    }

}