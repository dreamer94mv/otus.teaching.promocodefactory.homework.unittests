using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers
{
    public static class InMemoryConfiguration
    {
        public static IServiceProvider GetServiceProvider()
        {
            var services = new ServiceCollection();
            services
                .AddScoped(typeof(IRepository<>), typeof(EfRepository<>))
                .AddScoped<IDbInitializer, EfDbInitializer>();
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();
            services.AddDbContext<DataContext>(options =>
            {
                options.UseInMemoryDatabase("inMemoryDb");
                options.UseInternalServiceProvider(serviceProvider);
            });
            services.AddTransient<DbContext, DataContext>();

            return services.BuildServiceProvider();
        }
    }
}