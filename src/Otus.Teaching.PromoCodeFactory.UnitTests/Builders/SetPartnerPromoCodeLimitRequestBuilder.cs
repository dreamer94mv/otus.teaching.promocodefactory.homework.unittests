using System;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    static class SetPartnerPromoCodeLimitRequestBuilder
    {
        public static SetPartnerPromoCodeLimitRequest CreateSetPartnerPromoCodeLimitRequestValid()
        {
            return CreateSetPartnerPromoCodeLimitRequest(1000);
        }

        public static SetPartnerPromoCodeLimitRequest CreateSetPartnerPromoCodeLimitRequest(int limitValue)
        {
            return new SetPartnerPromoCodeLimitRequest()
            {
                Limit = limitValue,
                EndDate = DateTime.Today.AddDays(1)
            };
        }
    }
}