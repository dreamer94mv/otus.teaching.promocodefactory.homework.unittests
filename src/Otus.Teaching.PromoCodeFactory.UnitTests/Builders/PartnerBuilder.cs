using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public static class PartnerBuilder
    {
        public static Guid CreateNotExistGuid()
        {
            return Guid.Parse("def47942-7aaf-44a1-ae21-05aa4948b165");
        }

        public static Partner CreateNullPartner()
        {
            return null;
        }

        public static Partner CreatePartnerIsNotActive()
        {
            return CreateBasePartner().WithIsNotActive();
        }

        public static Partner WithIsNotActive(this Partner partner)
        {
            partner.IsActive = false;
            return partner;
        }
        
        public static Partner WithoutLimit(this Partner partner)
        {
            foreach (var partnerPromoCodeLimit in partner.PartnerLimits)
                partnerPromoCodeLimit.CancelDate ??= DateTime.Today;
            
            return partner;
        }
        
        public static Partner ClearLimits(this Partner partner)
        {
            partner.PartnerLimits.Clear();
            return partner;
        }
        
        public static Partner CreateBasePartner()
        {
            return new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Партнер",
                IsActive = true,
                NumberIssuedPromoCodes = 10,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };

        }

        public static Partner CreatePartnerWithWithoutAvailableLimit(bool withAvailableLimit)
        {
            var ret = CreateBasePartner();
            if (!withAvailableLimit)
                ret = ret.WithoutLimit();
            return ret;
        }
    }

}