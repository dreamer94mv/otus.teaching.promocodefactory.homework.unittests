using System;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public class SetPartnerPromoCodeLimitArrangeContext
    {
        public Guid PartnerId { get; set; }
        public SetPartnerPromoCodeLimitRequest Request { get; set; }

    }
}