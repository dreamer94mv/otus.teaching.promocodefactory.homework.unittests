using System;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public static class SetPartnerPromoCodeLimitAsyncArrangeBuilder
    {
        public static SetPartnerPromoCodeLimitArrangeContext SetupPartnerNotFoundArrange(
            Mock<IRepository<Partner>> partnersRepositoryMock)
        {
            return SetupPartnerBaseArrange(partnersRepositoryMock, PartnerBuilder.CreateNotExistGuid(),
                PartnerBuilder.CreateNullPartner(), SetPartnerPromoCodeLimitRequestBuilder.CreateSetPartnerPromoCodeLimitRequestValid());
        }
        
        public static SetPartnerPromoCodeLimitArrangeContext SetupPartnerIsNotActiveArrange(
            Mock<IRepository<Partner>> partnersRepositoryMock)
        {
            Partner partner = PartnerBuilder.CreatePartnerIsNotActive();
            return SetupPartnerBaseArrange(partnersRepositoryMock, partner.Id,
                partner, SetPartnerPromoCodeLimitRequestBuilder.CreateSetPartnerPromoCodeLimitRequestValid());
        }

        public static SetPartnerPromoCodeLimitArrangeContext SetupPartnerBaseArrange(
            Mock<IRepository<Partner>> partnersRepositoryMock, Guid partnerId, Partner partner,
            SetPartnerPromoCodeLimitRequest setPartnerPromoCodeLimitRequest)
        {
            partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            return new SetPartnerPromoCodeLimitArrangeContext { PartnerId = partnerId, Request = setPartnerPromoCodeLimitRequest};
        }

        public static SetPartnerPromoCodeLimitArrangeContext SetupPartnerWithWithoutAvailableLimit(Mock<IRepository<Partner>> partnersRepositoryMock, bool withAvailableLimit)
        {
            Partner partner = PartnerBuilder.CreatePartnerWithWithoutAvailableLimit(withAvailableLimit);
            Guid partnerId = partner.Id;
            return SetupPartnerBaseArrange(partnersRepositoryMock, partnerId, partner,
                SetPartnerPromoCodeLimitRequestBuilder.CreateSetPartnerPromoCodeLimitRequestValid());
        }

        public static SetPartnerPromoCodeLimitArrangeContext SetupPartnerWithExistAndNewLimits(Mock<IRepository<Partner>> partnersRepositoryMock)
        {
            Partner partner = PartnerBuilder.CreatePartnerWithWithoutAvailableLimit(true);
            Guid partnerId = partner.Id;
            return SetupPartnerBaseArrange(partnersRepositoryMock, partnerId, partner,
                SetPartnerPromoCodeLimitRequestBuilder.CreateSetPartnerPromoCodeLimitRequestValid());
        }

        public static SetPartnerPromoCodeLimitArrangeContext SetupPartnerWithSpecificLimit(Mock<IRepository<Partner>> partnersRepositoryMock, int newLimitValue)
        {
            Partner partner = PartnerBuilder.CreateBasePartner();
            Guid partnerId = partner.Id;
            return SetupPartnerBaseArrange(partnersRepositoryMock, partnerId, partner,
                SetPartnerPromoCodeLimitRequestBuilder.CreateSetPartnerPromoCodeLimitRequest(newLimitValue));
        }
    }

}